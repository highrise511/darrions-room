Images = new FS.Collection("images", {
  stores: [
    new FS.Store.GridFS("original")
  ],
  filter: {
    allow: {
      contentTypes: ['image/*']
    }
  }
});
 
if (Meteor.isServer) {
  Images.allow({
    insert: function (userId) {
    	console.log("insert");
    	return true;
    },
    remove: function (userId) {
    	console.log("remove");
    	return true;
    },
    download: function () {
    	console.log("download");
    	return true;
    },
    update: function (userId) {
    	console.log("update");
    	return true;
    }
  });
 
  Meteor.publish('images', function() {
    return Images.find({});
  });
}