Blogs = new Mongo.Collection("blogs");
//[ 'admin', 'user-admin' ]
Blogs.allow({
  insert: function (userId, blog) {
    return userId && blog.owner === userId;
  },
  update: function (userId, blog, fields, modifier) {
    if (userId !== blog.owner || Meteor.user().roles.indexOf('admin') < 0)
      return false;

    return true;
  },
  remove: function (userId, blog) {
    if (userId !== blog.owner || Meteor.user().roles.indexOf('admin') < 0)
      return false;

    return true;
  }
});

Meteor.methods({
  updateBlogLikes: function(bId) {
	var uId = Meteor.userId();
	if(uId){
		if(isLiked(bId,uId)){
			Blogs.update({ 
		        "_id": bId, 
		        "likes": uId
		    },
		    {
		        "$inc": { "likeCount": -1 },
		        "$pull": { "likes": uId }
		    });
		}else{		
		    Blogs.update({ 
		        "_id": bId, 
		        "likes": { "$ne": uId }
		    },
		    {
		        "$inc": { "likeCount": 1 },
		        "$push": { "likes": uId }
		    });
		}  
	}
  }
});

var isLiked = function(bId, uId){
	var fc = Blogs.find(
	    { 
	    	"_id": bId, 
	    	"likes": uId 
	    }
	)
	return fc.count() > 0;
}
