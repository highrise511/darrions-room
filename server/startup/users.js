Meteor.publish("users", function () {
  return Meteor.users.find({}, {fields: {emails: 1, profile: 1, username:1}});
});
Meteor.publish("userData", function () {
  return Meteor.users.find({ _id: this.userId }, {fields: {emails: 1, profile: 1, roles: 1}});
});