angular.module("interviews").directive('profiles', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/profiles/profiles.ng.html',
    controllerAs: 'profilesList',
    controller: function ($scope, $reactive, $state, $mdDialog, $filter) {
      $reactive(this).attach($scope);
      var self = this;
      
//      self.subscribe('images');
      self.subscribe('profiles');
      
      self.getMainImage = (images) => {
//          if (images && images.length) {
//            var url = $filter('filter')(self.images, {_id: images[0]})[0].url();
//            return {
//            	'background-image': 'url("' + url + '")'
//            }
//          }
        };
      
      self.newProfile = {};
      
      self.helpers({
        profiles: () => {
          return Profiles.find({});
        },
        users: () => {
          return Meteor.users.find({});
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        },
        images: () => {
          return Images.find({});
        }
      });
      
      self.openAddNewProfileModal = function () {
          $mdDialog.show({
            template: '<add-new-profile-modal></add-new-profile-modal>',
            clickOutsideToClose: true
          });
        };
        
      self.getUserById = (userId) => {	
        return Meteor.users.findOne(userId);
      };
      
      self.addProfile = () => {
		  this.newProfile.owner = Meteor.user()._id;
		  Profiles.insert(this.newProfile);
		  this.newProfile = {};
	  };
		
	  self.removeProfile = (profile) => {
		  Profiles.remove({_id: profile._id});
 	  } 
 
    }
  }
});