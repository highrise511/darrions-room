angular.module('interviews').directive('profileDetails', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/profiles/profile-details/profile-details.ng.html',
    controllerAs: 'profileDetail',
    controller: function ($scope, $stateParams, $reactive) {
    	
      $reactive(this).attach($scope);
      
      var self = this;
      
      self.subscribe('profiles');
 
      self.helpers({
        profile: () => {
          return Profiles.findOne({_id: $stateParams.profileId});
        }
      });
       
      self.save = () => {
        Profiles.update({_id: $stateParams.profileId}, {
          $set: {
            firstName: self.profile.firstName,
            lastName: self.profile.lastName
          }
        }, (error) => {
          if (error) {
            console.log('Oops, unable to update the profile...');
          }
          else {
            console.log('Done!');
          }
        });
      };
    }
  }
});