angular.module("interviews").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('login', {
        url: '/login',
        template: '<login></login>'
      })
      .state('register', {
        url: '/register',
        template: '<register></register>'
      })
      .state('resetpw', {
        url: '/resetpw',
        template: '<resetpw></resetpw>'
      })
      .state('profiles', {
        url: '/profiles',
        template: '<profiles></profiles>',
        resolve: {
            currentUser: ($q) => {
              if (Meteor.userId() == null) {
                return $q.reject('AUTH_REQUIRED');
              }
              else {
                return $q.resolve();
              }
            }
          }
      })
      .state('profileDetails', {
        url: '/profiles/:profileId',
        template: '<profile-details></profile-details>'
      })
      .state('profilelist', {
          url: '/profilelist',
          templateUrl: 'client/profiles/views/profile-list.ng.html',
          controller: 'ProfileListCtrl'
        })
      .state('about', {
        url: '/about',
        templateUrl: 'client/about/views/about-details.ng.html'
      });

    $urlRouterProvider.otherwise("/login");
}]);
  
angular.module("interviews").run(["$rootScope", "$state", function($rootScope, $state) {
	
	$rootScope.$on('$stateChangeSuccess', 
		function(event, toState, toParams, fromState, fromParams){ 
	    var cc = document.getElementById('mainContent');
	    cc.scrollIntoView();
	    cc.scrollTop=0;
	});
  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireUser promise is rejected
    // and redirect the user back to the main page
    if (error === "AUTH_REQUIRED") {
      $state.go('login');
    }
  });
}]);
