angular.module('interviews').directive('navitems', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/lib/navitems.ng.html',
    controllerAs: 'navItems',
    controller: function ($scope, $reactive, $mdSidenav) {
      $reactive(this).attach($scope);
      
      $scope.close = function () {
          $mdSidenav('left').close()
            .then(function () {
              //$log.debug("close LEFT is done");
            });
        };
      

//      this.helpers({
//        routes: () => {
//          return Routes.find({});
//        }
//      });
    }
  }
});