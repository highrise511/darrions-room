(function () {
	
	angular.module('interviews',[
	 'angular-meteor',
	 'ui.router',
	 'accounts.ui',
	 'angularUtils.directives.dirPagination',
	 'ngMaterial',
	 'ngFileUpload',
	 'ngImgCrop',
	 'xeditable',
	 'angular-sortable-view'])
	.config(['$mdThemingProvider',function($mdThemingProvider) {
	  $mdThemingProvider
	  	.theme('default')
	  	.accentPalette('orange')
	    .backgroundPalette('grey');
	}]);
	angular.module('interviews').config(['$mdIconProvider', function ($mdIconProvider) {
		  $mdIconProvider
		    .iconSet("social", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-social.svg")
		    .iconSet("action", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg")
		    .iconSet("communication", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-communication.svg")
		    .iconSet("content", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg")
		    .iconSet("toggle", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-toggle.svg")
		    .iconSet("navigation", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg")
		    .iconSet("image", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg");
		}]);
	angular.module("interviews").filter('reverse', function() {
	  return function(items) {
		  if(items){
			  return items.slice().reverse();
		  }
	  };
	});	
	Meteor.subscribe('userData');
	function onReady() {
	  angular.bootstrap(document, ['interviews']);
	}
})();