angular.module('interviews').directive('myroom', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/myroom/myroom.html',
    controllerAs: 'myroom',
    controller: function ($scope, $reactive) {
      $reactive(this).attach($scope);

      this.helpers({
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUser: () => {
          return Meteor.user();
        }
      });

      this.logout = () => {
        Accounts.logout();
      }
    }
  }
});