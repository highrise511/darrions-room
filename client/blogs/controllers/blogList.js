angular.module("interviews").controller("BlogListCtrl", ['$scope', '$meteor', '$sce', '$mdMedia', '$location', '$mdDialog',
  function($scope, $meteor, $sce, $mdMedia, $location, $mdDialog){

    $scope.sort = { date: -1 };
    $scope.orderProperty = '1';

    $meteor.collection(Blogs).subscribe('blogs');
    $scope.blogs = $meteor.collection(function() {
      return Blogs.find({}, {
        sort : $scope.getReactively('sort')
      });
    });
    
    $scope.likeBlog = function(blog){
    	Meteor.call('updateBlogLikes', blog._id);
    }
    
    $scope.remove = function(blog){
      if($scope.confirmDelete()) $scope.blogs.splice( $scope.blogs.indexOf(blog), 1 );
    };

    $scope.removeAll = function(){
      $scope.blogs.remove();
    };
    
    $scope.getAvatarImgUrl = function(blog) {
		var imgName = "bgs/logosquare.jpg";
		if (blog && blog.type && blog.type != "") {imgName = blog.type;};
		if(blog && blog.thumb && blog.thumb !==""){imgName = "thumbs/" + blog.thumb;};
		var imgUrl = "http://demo.cloudimg.io/s/height/400/http://creatingconvo.com/" + imgName;
		imgUrl = $scope.getMdImgUrl(imgName);
    	return imgUrl;
    };
    
    $scope.avatarstyle = function(blog){return {'background-image': 'url(' + $scope.getAvatarImgUrl(blog) + ')', 'background-repeat': 'no-repeat', 'background-size' : 'cover', 'background-position': 'center','height':'200px'}};
    
    $scope.go = function ( path ) {
    	  $location.path( path );
	};
    
    $scope.showAdvanced = function(ev) {
    	console.log('show dialog');
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'client/blogs/views/newBlogDialog.ng.html',
          targetEvent: ev,
        })
        .then(function(newBlog) {
        	newBlog.likes=[];
        	newBlog.likeCount=0;
        	$scope.blogs.push(newBlog);
        	console.log('saved new blog');
        }, function() {
        	console.log('cancelled new post');
        });
      };
      
}]);

function DialogController($scope, $mdDialog, $meteor, $sce) {
	$scope.Date = new Date();
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
	  $mdDialog.cancel();
	};
	$scope.answer = function(newBlog) {
	  $mdDialog.hide(newBlog);
	};
	$scope.trustSrc = function(src) {
	  return $sce.trustAsResourceUrl(src);
	}
}
DialogController.$inject = ['$scope', '$mdDialog', '$meteor', '$sce']