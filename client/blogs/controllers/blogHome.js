angular.module("interviews").controller("BlogHomeCtrl", ['$scope', '$stateParams', '$rootScope', '$meteor', '$location', 
  function($scope, $stateParams, $rootScope, $meteor, $location){
	var bId = '4';
	$scope.page = 1;
    $scope.perPage = 10;
    $scope.sort = { date: -1 };
    $scope.orderProperty = '-1';
    $scope.search = bId;

    $meteor.subscribe('users');
    var defaultLink = "https://www.youtube.com/embed/mqEve9KoFls?loop=1";
	$scope.blog = {
			name:"2015 Post Production Reel CC - Creating Conversation", 
			link:defaultLink
	};
	$scope.mainurl= $scope.blog.link;
	
    $scope.comments = $meteor.collection(function() {
      return Comments.find({blogId:bId}, {
        sort : $scope.getReactively('sort')
      });
    });
   
   $scope.remove = function(comment){
      if($scope.confirmDelete())$scope.comments.splice( $scope.comments.indexOf(comment), 1 );
    };
	    
    $scope.addComment=function(newComment, root){
    	newComment.blogId=bId;
    	newComment.owner=root.currentUser._id;
    	newComment.date=new Date();
    	newComment.likes=[];
    	newComment.likeCount=0;
    	$scope.comments.push(newComment);
    	$scope.newComment='';
    }
    
    $scope.likeComment = function(comment){
    	Meteor.call('updateCommentLikes', comment._id);
    };
    
    $scope.likeBlog = function(blog){
    	Meteor.call('updateBlogLikes', blog._id);
    }

    $scope.getUserById = function(userId){
      return Meteor.users.findOne(userId);
    };

    $scope.creator = function(comment){
      if (!comment)
        return;
      var owner = $scope.getUserById(comment.owner);
      if (!owner)
        return "nobody";

      if ($rootScope.currentUser)
        if ($rootScope.currentUser._id)
          if (owner._id === $rootScope.currentUser._id)
            return "me";

      return owner;
    };
    
    $scope.creator = function(comment){
	    if (!comment)
	      return;
	    var owner = $scope.getUserById(comment.owner);
	    if (!owner)
	      return "nobody";
	
	    if ($rootScope.currentUser)
	      if ($rootScope.currentUser._id)
	        if (owner._id === $rootScope.currentUser._id)
	          return "me";
	    return owner;
	  };
   
   $meteor.autorun($scope, function() {
      $meteor.subscribe('comments', {
        limit: parseInt($scope.getReactively('perPage')),
        skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage')),
        sort: $scope.getReactively('sort')
      }, $scope.getReactively('search')).then(function() {
        $scope.commentsCount = $meteor.object(Counts ,'numberOfComments', false);
        
      });
    });
   
   $scope.pageChanged = function(newPage) {
      $scope.page = newPage;
    };
   
}]);
