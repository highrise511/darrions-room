angular.module("interviews").controller("BlogDetailsCtrl", ['$scope', '$stateParams', '$rootScope', '$meteor', '$location', 
  function($scope, $stateParams, $rootScope, $meteor, $location){
	
	$scope.page = 1;
    $scope.perPage = 10;
    $scope.sort = { date: 1 };
    $scope.orderProperty = '1';
    $scope.search = $stateParams.blogId;

    $meteor.subscribe('users');
//   $scope.users = $meteor.collection(Meteor.users, false);
   $scope.blog = $meteor.object(Blogs, $stateParams.blogId).subscribe('blogs');
//   $scope.comments = $meteor.object(Comments, $stateParams.blogId).subscribe('comments');
   $scope.comments = $meteor.collection(function() {
      return Comments.find({blogId:$stateParams.blogId}, {
        sort : $scope.getReactively('sort')
      });
    });
   
   $scope.remove = function(comment){
      if($scope.confirmDelete())$scope.comments.splice( $scope.comments.indexOf(comment), 1 );
    };
	    
    $scope.addComment=function(newComment, root){
    	newComment.blogId=$stateParams.blogId;
    	newComment.owner=root.currentUser._id;
    	newComment.date=new Date();
    	newComment.likes=[];
    	newComment.likeCount=0;
    	$scope.comments.push(newComment);
    	$scope.newComment='';
    }
    
    $scope.likeComment = function(comment){
    	Meteor.call('updateCommentLikes', comment._id);
    };
    
    $scope.likeBlog = function(blog){
    	Meteor.call('updateBlogLikes', blog._id);
    }

    $scope.getUserById = function(userId){
      return Meteor.users.findOne(userId);
    };

    $scope.creator = function(comment){
      if (!comment)
        return;
      var owner = $scope.getUserById(comment.owner);
      if (!owner)
        return "nobody";

      if ($rootScope.currentUser)
        if ($rootScope.currentUser._id)
          if (owner._id === $rootScope.currentUser._id)
            return "me";

      return owner;
    };
    
    $scope.creator = function(comment){
	    if (!comment)
	      return;
	    var owner = $scope.getUserById(comment.owner);
	    if (!owner)
	      return "nobody";
	
	    if ($rootScope.currentUser)
	      if ($rootScope.currentUser._id)
	        if (owner._id === $rootScope.currentUser._id)
	          return "me";
	
	    return owner;
	  };
   
   $meteor.autorun($scope, function() {
	      $meteor.subscribe('comments', {
	        limit: parseInt($scope.getReactively('perPage')),
	        skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage')),
	        sort: $scope.getReactively('sort')
	      }, $scope.getReactively('search')).then(function() {
	        $scope.commentsCount = $meteor.object(Counts ,'numberOfComments', false);
	        
	      });
	    });
   
   $scope.go = function ( path ) {
	  	  $location.path( path );
	  	};
   
}]);
