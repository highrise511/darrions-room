angular.module("interviews").controller("UserListCtrl", ['$scope', '$meteor', '$location', '$mdDialog', '$window',
  function($scope, $meteor, $location, $mdDialog, $window){

	$scope.users = $meteor.collection(Meteor.users, false).subscribe('users');

    $scope.remove = function(user){
      $scope.users.splice( $scope.users.indexOf(user), 1 );
    };

    $scope.removeAll = function(){
      $scope.users.remove();
    };
    
    $scope.go = function ( path ) {
	  $location.path( path );
	};
    
    $scope.showAdvanced = function(ev) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'client/users/views/newUserDialog.ng.html',
          targetEvent: ev,
        })
        .then(function(newUser) {
        	$scope.users.push(newUser);
        	console.log('saved new user');
        }, function() {
        	console.log('cancelled new user');
        });
      };
}]);

function DialogController($scope, $mdDialog, $meteor) {
	$scope.Date = new Date();
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
	  $mdDialog.cancel();
	};
	$scope.answer = function(newUser) {
	  $mdDialog.hide(newUser);
	};
}
DialogController.$inject = ['$scope', '$mdDialog', '$meteor']